export const fruitMixin = {
    data() {
        return {
            fruits: ['Apple', 'Orange', 'Lemon', 'Mango', 'Melon'],
            filterText: ''
        }
    },
    // filters is a good solution, but computed properties are a better solution
    computed: {
        // computed propertie to return computed array all the time with a filtered value
        filteredFruits() {
            return this.fruits.filter((el) => {
                return el.match(this.filterText);
            });
        }
    },
    created() {
        console.log('Created');
    }
}