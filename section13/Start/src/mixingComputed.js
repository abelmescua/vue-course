export const mixingComputed = {
    computed: {
        reverseText() {
            return this.text.split("").reverse().join("");
        },
        amountText() {
            return this.text + ' (' + this.text.length + ')';
        }
    }
};