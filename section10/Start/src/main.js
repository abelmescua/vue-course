import Vue from 'vue'
import App from './App.vue'

export const eventBus = new Vue({
  methods: {
    insertQuote(quote) {
      this.$emit('quoteInserted', quote);
    },
    deleteQuote(index) {
      this.$emit('quoteDeleted', index);
    }
  }
});

new Vue({
  el: '#app',
  render: h => h(App)
})
